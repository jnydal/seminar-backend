package no.thor.seminarservice.exception;

public class BusinessProcessException extends Exception {

    public BusinessProcessException(String message) {
        super(message);
    }

}
