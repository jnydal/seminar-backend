package no.thor.seminarservice.controller;

import no.thor.seminarservice.controller.dto.CourseDto;
import no.thor.seminarservice.entity.CourseEntity;
import no.thor.seminarservice.service.CourseService;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@Component
@RestController
public class CourseRestEndpoint implements ErrorController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private CourseService courseService;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping("/courses")
    public List getAllCourses() {
        return courseService.getAllCourses().stream()
                .map(course -> convertToDto(course))
                .collect(Collectors.toList());
    }

    @GetMapping("/courses/search/{query}")
    public List searchName(@PathVariable("query") String query) {
        return courseService.getByName(query).stream()
                .map(course -> convertToDto(course))
                .collect(Collectors.toList());
    }

    @GetMapping("/courses/{courseId}")
    public ResponseEntity getCourse(@PathVariable("courseId") long courseId) {

        CourseDto courseDto = convertToDto(courseService.get(courseId));
        if (courseDto == null) {
           return new ResponseEntity("No course found for id " + courseId, HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity(courseDto, HttpStatus.OK);

    }

    @PostMapping(value = "/courses")
    public ResponseEntity createCourse(@RequestBody CourseDto courseDto) {

        courseService.create(convertToEntity(courseDto));
        return new ResponseEntity(courseDto, HttpStatus.OK);

    }

    @RequestMapping(value = "/courses/create", method = RequestMethod.POST)
    public ResponseEntity<CourseDto> create(@RequestParam("name") String name,
                                            @RequestParam("room") String room,
                                            @RequestParam("instructor") String instructor,
                                            @RequestParam("startsDateString") String startsDateString,
                                            @RequestParam("endsDateString") String endsDateString) {
        CourseDto course = new CourseDto();
        course.setName(name);
        course.setRoom(room);
        course.setInstructor(instructor);
        course.setStartsString(startsDateString);
        course.setEndsString(endsDateString);
        courseService.create(convertToEntity(course));
        return new ResponseEntity<CourseDto>(course, HttpStatus.OK);
    }

    @DeleteMapping("/courses/{courseId}")
    public ResponseEntity deleteCourse(@PathVariable long courseId) {

        courseService.delete(courseId);
        return new ResponseEntity(courseId, HttpStatus.OK);

    }

    @PutMapping("/courses/{courseId}")
    public ResponseEntity updateCourse(@PathVariable String courseId, @RequestBody CourseDto courseDto) {

        courseService.update(convertToEntity(courseDto));

        if (null == courseDto) {
            return new ResponseEntity("No course found for Id " + courseId, HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity(courseDto, HttpStatus.OK);
    }

    private CourseEntity convertToEntity(CourseDto courseDto) {
        CourseEntity courseEntity = new CourseEntity();
        DateFormat format = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
        Date start = null;
        Date end = null;
        try {
            start = format.parse(courseDto.getStartsString());
            end = format.parse(courseDto.getEndsString());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        courseEntity.setStarts(start);
        courseEntity.setEnds(end);
        return courseEntity;
    }

    private CourseDto convertToDto(CourseEntity courseEntity) {
        CourseDto courseDto = new CourseDto();
        courseDto.setName(courseEntity.getName().toLowerCase());
        courseDto.setRoom(courseEntity.getRoom());
        courseDto.setInstructor(courseEntity.getInstructor());
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        courseDto.setStartsString(df.format(courseEntity.getStarts()));
        courseDto.setEndsString(df.format(courseEntity.getEnds()));
        return courseDto;
    }

    @Override
    public String getErrorPath() {
        return "/error";
    }
}