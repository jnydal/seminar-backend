package no.thor.seminarservice.controller.dto;

import java.util.Date;

public class CourseDto {

    private long id;

    private String name;

    private String instructor;

    private String room;

    private String startsString;

    private String endsString;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInstructor() {
        return instructor;
    }

    public void setInstructor(String instructor) {
        this.instructor = instructor;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public String getStartsString() {
        return startsString;
    }

    public void setStartsString(String startsString) {
        this.startsString = startsString;
    }

    public String getEndsString() {
        return endsString;
    }

    public void setEndsString(String endsString) {
        this.endsString = endsString;
    }
}
