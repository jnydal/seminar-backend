package no.thor.seminarservice.controller.dto;

import no.thor.seminarservice.entity.CourseEntity;

import java.util.Date;

public class CourseParticipiantDto {

    private CourseEntity course;

    private String name;

    public CourseEntity getCourse() {
        return course;
    }

    public void setCourse(CourseEntity course) {
        this.course = course;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
