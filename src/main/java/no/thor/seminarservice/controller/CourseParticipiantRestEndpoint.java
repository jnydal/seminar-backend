package no.thor.seminarservice.controller;

import no.thor.seminarservice.controller.dto.CourseDto;
import no.thor.seminarservice.controller.dto.CourseParticipiantDto;
import no.thor.seminarservice.entity.CourseEntity;
import no.thor.seminarservice.entity.CourseParticipiantEntity;
import no.thor.seminarservice.service.CourseService;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@Component
@RestController
public class CourseParticipiantRestEndpoint implements ErrorController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private CourseService courseService;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping("/course_participiants")
    public List getAllCourses() {
        logger.warn("accessing courses");
        return courseService.getAllCourseParticipiants().stream()
                .map(course -> convertToDto(course))
                .collect(Collectors.toList());
    }

    @GetMapping("/course_participiant/search/{query}")
    public List searchName(@PathVariable("query") String query) {
        return courseService.getByParticipiantName(query).stream()
                .map(course -> convertToDto(course))
                .collect(Collectors.toList());
    }

    @GetMapping("/course_participiants/{participiantId}")
    public ResponseEntity getCourseParticipiant(@PathVariable("courseParticipiantId") long courseParticipiantId) {

        CourseParticipiantDto courseParticipiantDto = convertToDto(courseService.getParticipiant(courseParticipiantId));
        if (courseParticipiantDto == null) {
           return new ResponseEntity("No participiant found for id " + courseParticipiantId, HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity(courseParticipiantDto, HttpStatus.OK);

    }

    @PostMapping(value = "/course_participiants")
    public ResponseEntity createCourse(@RequestBody CourseParticipiantDto courseParticipiantDto) {

        courseService.createParticipiant(convertToEntity(courseParticipiantDto));
        return new ResponseEntity(courseParticipiantDto, HttpStatus.OK);

    }

    @RequestMapping(value = "/course_participiants/create", method = RequestMethod.POST)
    public ResponseEntity<CourseParticipiantDto> create(@RequestParam("name") String name,
                                            @RequestParam("room") String room,
                                            @RequestParam("instructor") String instructor,
                                            @RequestParam("startTime") String startTime,
                                            @RequestParam("endTime") String endTime) {
        CourseParticipiantDto participiant = new CourseParticipiantDto();
        participiant.setName(name);
        DateFormat format = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
        Date start = null;
        Date end = null;
        try {
            start = format.parse(startTime);
            end = format.parse(endTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        courseService.createParticipiant(convertToEntity(participiant));
        return new ResponseEntity<CourseParticipiantDto>(participiant, HttpStatus.OK);
    }

    @DeleteMapping("/course_participiants/{participiantId}")
    public ResponseEntity deleteCourse(@PathVariable long participiantId) {
        courseService.deleteParticipiant(participiantId);
        return new ResponseEntity(participiantId, HttpStatus.OK);

    }

    @PutMapping("/course_participiant/{participiantId}")
    public ResponseEntity updateCourse(@PathVariable String participiantId, @RequestBody CourseParticipiantDto courseParticipiantDto) {

        courseService.updateParticipiant(convertToEntity(courseParticipiantDto));

        if (null == courseParticipiantDto) {
            return new ResponseEntity("No participiant found for Id " + participiantId, HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity(courseParticipiantDto, HttpStatus.OK);
    }

    private CourseParticipiantEntity convertToEntity(CourseParticipiantDto courseDto) {
        CourseParticipiantEntity courseParticipiantEntity = modelMapper.map(courseDto, CourseParticipiantEntity.class);
        courseParticipiantEntity.setName(courseDto.getName());
        return courseParticipiantEntity;
    }

    private CourseParticipiantDto convertToDto(CourseParticipiantEntity courseEntity) {
        CourseParticipiantDto courseDto = modelMapper.map(courseEntity, CourseParticipiantDto.class);
        courseDto.setName(courseEntity.getName());
        return courseDto;
    }

    @Override
    public String getErrorPath() {
        return "/error";
    }
}