package no.thor.seminarservice.service;

import no.thor.seminarservice.entity.CourseEntity;
import no.thor.seminarservice.entity.CourseParticipiantEntity;

import java.util.List;

public interface CourseService {

    public List<CourseEntity> getAllCourses();

    public List<CourseEntity> getByName(String email);

    public void create(CourseEntity courseEntity);

    public void delete(long id);

    public void update(CourseEntity courseEntity);

    public CourseEntity get(long courseId);

    public CourseParticipiantEntity getParticipiant(long courseParticipiantId);

    public List<CourseParticipiantEntity> getAllCourseParticipiants();

    public List<CourseParticipiantEntity> getByParticipiantName(String name);

    public void createParticipiant(CourseParticipiantEntity courseParticipiantEntity);

    public void deleteParticipiant(long id);

    public void updateParticipiant(CourseParticipiantEntity courseParticipiantEntity);

}
