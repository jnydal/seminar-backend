package no.thor.seminarservice.service;

import no.thor.seminarservice.entity.CourseEntity;
import no.thor.seminarservice.entity.CourseParticipiantEntity;
import no.thor.seminarservice.exception.BusinessProcessException;
import no.thor.seminarservice.repository.CourseParticipiantRepository;
import no.thor.seminarservice.repository.CourseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(rollbackFor={BusinessProcessException.class})
public class CourseServiceImpl implements CourseService {

    @Autowired
    private CourseRepository courseRepository;

    @Autowired
    private CourseParticipiantRepository courseParticipiantRepository;

    @Override
    public List<CourseEntity> getAllCourses() {
        return courseRepository.findAll();
    }

    @Override
    public List<CourseEntity> getByName(String name) {
        return courseRepository.findByName(name);
    }

    @Override
    public void create(CourseEntity courseEntity) {
        courseRepository.saveAndFlush(courseEntity);
    }

    @Override
    public void delete(long id) {
        courseRepository.delete(id);
    }

    @Override
    public void update(CourseEntity courseEntity) {
        courseRepository.saveAndFlush(courseEntity);
    }

    @Override
    public CourseEntity get(long courseId) {
        return courseRepository.findOne(courseId);
    }

    @Override
    public CourseParticipiantEntity getParticipiant(long courseParticipiantId) {
        return courseParticipiantRepository.getOne(courseParticipiantId);
    }

    @Override
    public List<CourseParticipiantEntity> getAllCourseParticipiants() {
        return courseParticipiantRepository.findAll();
    }

    @Override
    public List<CourseParticipiantEntity> getByParticipiantName(String name) {
        return courseParticipiantRepository.findByParticipiantName(name);
    }

    @Override
    public void createParticipiant(CourseParticipiantEntity courseParticipiantEntity) {
        courseParticipiantRepository.saveAndFlush(courseParticipiantEntity);
    }

    @Override
    public void deleteParticipiant(long id) {
        courseParticipiantRepository.delete(id);
    }

    @Override
    public void updateParticipiant(CourseParticipiantEntity courseParticipiantEntity) {
        courseParticipiantRepository.saveAndFlush(courseParticipiantEntity);
    }
}
