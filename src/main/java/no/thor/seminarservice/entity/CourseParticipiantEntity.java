package no.thor.seminarservice.entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "course_participiant")
public class CourseParticipiantEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="COURSE_ID")
    private CourseEntity course;

    private String name;

    public CourseEntity getCourse() {
        return course;
    }

    public void setCourse(CourseEntity course) {
        this.course = course;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
