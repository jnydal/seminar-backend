package no.thor.seminarservice.repository;

import no.thor.seminarservice.entity.CourseEntity;
import no.thor.seminarservice.entity.CourseParticipiantEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CourseParticipiantRepository extends JpaRepository<CourseParticipiantEntity, Long> {

    @Query("SELECT o FROM CourseParticipiantEntity o where o.name like LOWER(CONCAT('%',:name, '%'))")
    List<CourseParticipiantEntity> findByParticipiantName(@Param("name") String name);

}

