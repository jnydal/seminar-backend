package no.thor.seminarservice.repository;

import no.thor.seminarservice.entity.CourseEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CourseRepository extends JpaRepository<CourseEntity, Long> {

    @Query("SELECT o FROM CourseEntity o where o.name like LOWER(CONCAT('%',:name, '%'))")
    List<CourseEntity> findByName(@Param("name") String name);

}

