package no.thor.seminarservice.service;

import no.thor.seminarservice.entity.CourseEntity;
import no.thor.seminarservice.repository.CourseParticipiantRepository;
import no.thor.seminarservice.repository.CourseRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
public class CourseServiceImplTest {

    @TestConfiguration
    static class CourseServiceImplTestConfiguration {

        @Bean
        public CourseService courseService() {
            return new CourseServiceImpl();
        }
    }

    @Autowired
    private CourseService courseService;

    @MockBean
    private CourseParticipiantRepository courseParticipiantRepository;

    @MockBean
    private CourseRepository courseRepository;

    @Before
    public void setUp() {
        List<CourseEntity> result = new ArrayList<>();
        CourseEntity course = new CourseEntity();
        CourseEntity course2 = new CourseEntity();
        CourseEntity course3 = new CourseEntity();
        result.add(course);
        result.add(course2);
        result.add(course3);
        Mockito.when(courseService.getAllCourses()).thenReturn(result);
    }

    @Test
    public void shouldReturnAllCourses() {
        List<CourseEntity> result = new ArrayList<>();
        result = courseService.getAllCourses();
        assertEquals(3,result.size());
    }
}