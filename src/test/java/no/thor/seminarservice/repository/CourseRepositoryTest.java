package no.thor.seminarservice.repository;

import no.thor.seminarservice.entity.CourseEntity;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class CourseRepositoryTest {

    @Autowired
    CourseRepository repository;

    @Before
    public void setup() {
        CourseEntity course = new CourseEntity();
        course.setName("react.js");
        CourseEntity course2 = new CourseEntity();
        course2.setName("java8");
        CourseEntity course3 = new CourseEntity();
        course3.setName("spring");
        repository.save(course);
        repository.save(course2);
        repository.save(course3);
    }

    @After
    public void cleanup() {
        repository.deleteAll();
    }

    @Test
    public void shouldFindAllCourses() {
        Iterable<CourseEntity> courses = repository.findAll();
        assertThat(courses).hasSize(3);
    }

    @Test
    public void shouldStoreCourse() {
        CourseEntity course = new CourseEntity();
        course.setName("struts2");
        repository.save(course);
        assertThat(course).hasFieldOrPropertyWithValue("name", "struts2");
        Iterable<CourseEntity> courses = repository.findAll();
        assertThat(course).isIn(courses);
        assertThat(courses).hasSize(4);
    }

    @Test
    public void shouldDeleteCourse() {
        List<CourseEntity> courselist = repository.findByName("react.js");
        repository.delete(courselist.get(0));
        Iterable<CourseEntity> courses = repository.findAll();
        assertThat(courselist.get(0)).isNotIn(courses);
    }

    @Test
    public void shouldUpdateCourse() {
        CourseEntity course = repository.findByName("react.js").get(0);
        repository.save(course);
        course = repository.findByName("react.js").get(0);
        assertThat(course).hasFieldOrPropertyWithValue("name", "react.js");
    }
}