package no.thor.seminarservice.controller;

import no.thor.seminarservice.Application;
import no.thor.seminarservice.entity.CourseEntity;
import no.thor.seminarservice.repository.CourseParticipiantRepository;
import no.thor.seminarservice.repository.CourseRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        classes = Application.class)
@AutoConfigureMockMvc
@TestPropertySource("classpath:application-integrationtest.properties ")
public class CourseRestEndpointIT {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private MockMvc mvc;

    @Autowired
    private CourseRepository courseRepository;

    @Autowired
    private CourseParticipiantRepository courseParticipiantRepository;

    @Autowired
    Environment env;

    @Test
    public void shouldReturnCourseAfterAdding()
            throws Exception {

        logger.info(" ////// DATASOURCE: " + env.getProperty("spring.datasource.url"));

        CourseEntity c = new CourseEntity();
        c.setName("react.js");
        c.setInstructor("T");
        c.setRoom("A3");
        c.setStarts(parseDate("11-12-2018"));
        c.setEnds(parseDate("12-12-2018"));
        courseRepository.saveAndFlush(c);

        mvc.perform(get("/courses")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].name", is("react.js")));
    }

    public static Date parseDate(String date) {
        try {
            return new SimpleDateFormat("dd-MM-yyyy").parse(date);
        } catch (ParseException e) {
            return null;
        }
    }
}